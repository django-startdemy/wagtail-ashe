CURRENT_DIR=$(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))


.PHONY: default
default: install

.PHONY: install
install:
	pip install -r requirements.txt

.PHONY: runserver
runserver:
	python src/manage.py runserver 0.0.0.0:8000
